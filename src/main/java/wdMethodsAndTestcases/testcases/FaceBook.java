package wdMethodsAndTestcases.testcases;

import wdMethodsAndTestcases.wdMethods.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FaceBook extends SeMethods {

	@org.testng.annotations.BeforeClass
	public void DeclareTest()
	{
		Testcasename ="TC001FaceBook";
		Description = "faceBook Test leaf";
		author ="Sangeetha";
		category="Smoke";
	}

	@Test
	public void FaceBooks() throws InterruptedException {
		startApp("chrome", "https://www.facebook.com/");
		WebElement username = locateElement("id","email");
		type(username,"sangee_anna07@yahoo.co.in");
		WebElement password = locateElement("id","pass");
		type(password,"S@n@86may");
		WebElement login = locateElement("id","loginbutton");
		click(login);
		WebElement search = locateElement("xpath","//input[@name='q']");
		type(search,"TestLeaf");
		WebElement searchtext = locateElement("xpath","//ul[@id='facebar_typeahead_view_list']");
		clickWithNoSnap(searchtext);
		WebElement searchicon = locateElement("xpath","//button[@data-testid='facebar_search_button']");
		clickWithNoSnap(searchicon);
		clickWithNoSnap(searchicon);
		WebElement liketext = locateElement("xpath","(//button[@type='submit'])[2]");


		String lt=liketext.getText();
		if (lt.equalsIgnoreCase("like"))
		{
			click(liketext);
		}
		else
			System.out.println(liketext.getText());
		WebElement clickliketext = locateElement("xpath","//div[text()='TestLeaf']");  
		clickWithNoSnap(clickliketext);

		Thread.sleep(7000);

		System.out.println(driver.getTitle());


		WebElement likecount = locateElement("xpath","//div[contains(text(),'people like this')]");  
		System.out.println(likecount.getText());
				
		
	}	

}



